rm -rf ~/.pub-cache/
flutter pub-cache repair
find . -type f -name "*.g.dart" -print -delete && find . -type f -name "*.freezed.dart" -print -delete
flutter clean
rm pubspec.lock
flutter pub outdated
flutter pub get
